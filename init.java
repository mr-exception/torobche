package torobche;

import java.util.Random;

public class init {
	public static int[][] init_cube(int size, double dif) {
		int[][] space = new int[size][size];
		int rock_count = (int) (size * size * dif);
		Random rnd = new Random();
		up: for (int index = 0; index < rock_count; index++) {
			int row = rnd.nextInt(size);
			int col = rnd.nextInt(size);
			for (int i = Math.max(row - 1, 0); i < Math.min(row + 2, size); i++)
				for (int j = Math.max(col - 1, 0); j < Math.min(col + 2, size); j++)
					if (space[i][j] == -1)
						continue up;
			space[row][col] = -1;
		}
		while (true) {
			int x = rnd.nextInt(space.length);
			int y = rnd.nextInt(space.length);
			if (space[x][y] == 0) {
				space[x][y] = 1;
				break;
			}
		}
		return space;
	}

	public static void set_food(int[][] space) {
		Random rnd = new Random();
		while (true) {
			int x = rnd.nextInt(space.length);
			int y = rnd.nextInt(space.length);
			if (space[x][y] == 0) {
				space[x][y] = -2;
				break;
			}
		}
	}
}
