package torobche;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import torobche.graphic;
import torobche.init;
import torobche.simulate;

public class simulation_start {
	public int[][] space;

	public simulation_start(String path) throws IOException {
		Scanner in = new Scanner(new File(path));
		int port = 1212;
		double dif = 0.1;
		int size = 10;
		File log = new File("hs.txt");
		while (in.hasNextLine()) {
			String att = in.next();
			if (att.equals("port")) {
				String value = in.next();
				port = Integer.valueOf(value);
			} else if (att.equals("size")) {
				String value = in.next();
				size = Integer.valueOf(value);
			} else if (att.equals("dif")) {
				String value = in.next();
				dif = Double.valueOf(value);
			} else if (att.equals("log")) {
				String log_path = in.next();
				log = new File(log_path);
			}
		}
		System.out.println("started game by" + "\nport: " + port + "\nspace size: " + size + "\ndifficulity: " + dif
				+ "\nlog file: " + log.getName());
		space = init.init_cube(size, dif);
		init.set_food(space);
		graphic gr = new graphic(size, true);
		gr.show();
		gr.update_space(space);
		simulate sm = new simulate(port, space, gr, log);
		gr.set_sm(sm);
		sm.run();
	}

	public static void main(String[] args) throws IOException {
		new simulation_start("ini.conf");
	}
}
