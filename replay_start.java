package torobche;

import java.io.FileNotFoundException;

public class replay_start {
	public replay_start(String path) throws FileNotFoundException {
		log_reader lr = new log_reader(path);
		graphic gr = new graphic(lr.get_space_size(), false);
		gr.set_lr(lr);
		lr.set_graphic(gr);
		if (lr.log_size() > 0)
			gr.update_space(lr.get_log(0));
		gr.show();
	}

	public static void main(String[] args) throws FileNotFoundException {
		new replay_start("log.txt");
	}
}
