package torobche;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.Timer;

public class log_reader {
	private ArrayList<int[][]> history = new ArrayList<>();
	private int space_size;
	private int steps;
	private int foods;
	private double score;

	private graphic gr;

	public log_reader(String path) throws FileNotFoundException {
		Scanner in = new Scanner(new File(path));
		int size;
		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.equals("end;"))
				break;
			if (line.equals("") || line.equals("\n"))
				continue;
			String[] rows = line.split(";");
			size = rows.length;
			int[][] space = new int[size][size];
			for (int i = 0; i < size; i++) {
				String[] parts = rows[i].split(",");
				for (int j = 0; j < size; j++) {
					space[i][j] = Integer.valueOf(parts[j]);
				}
			}
			this.space_size = size;
			this.history.add(space);
		}
		while (in.hasNextLine()) {
			String line = in.nextLine();
			String[] parts = line.split(" ");
			if (parts[0].equals("step")) {
				String value = parts[1];
				steps = Integer.valueOf(value);
			} else if (parts[0].equals("passed_food")) {
				String value = parts[1];
				foods = Integer.valueOf(value);
			} else if (parts[0].equals("score")) {
				String value = parts[1];
				score = Double.valueOf(value);
			}
		}

	}

	public void set_graphic(graphic gr) {
		this.gr = gr;
		gr.set_log_status(steps, foods, score);
	}

	public int log_size() {
		return history.size();
	}

	public int get_space_size() {
		return space_size;
	}

	public int[][] get_log(int index) {
		return history.get(index);
	}

	public void play() {
		play_timeline.start();
	}

	public void pause() {
		play_timeline.stop();
	}

	public void stop() {
		play_timeline.stop();
		time_step = 0;
	}

	public void next() {
		if (time_step < history.size() - 1) {
			time_step++;
			gr.update_space(history.get(time_step));
		}
	}

	public void prev() {
		if (time_step > 0) {
			time_step--;
			gr.update_space(history.get(time_step));
		}
	}

	private int time_step = 0;
	private Timer play_timeline = new Timer(200, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (history.size() > time_step) {
				gr.update_space(history.get(time_step));
				time_step++;
			} else {
				gr.restart();
				time_step = 0;
			}
		}
	});
}
