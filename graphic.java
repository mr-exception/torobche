package torobche;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class graphic {
	private int size;
	private int cell_h;
	private JFrame mf;
	private JPanel[][] space_label;
	private JPanel space_panel;

	private JLabel status;
	private JLabel score;
	private JLabel steps;
	private JLabel passed_foods;

	// on replay logs
	private JButton next;
	private JButton prev;
	private JButton play;
	private JButton stop;
	private boolean playing = false;

	private boolean live;
	private log_reader lr;
	private simulate sm;

	public void set_lr(log_reader lr) {
		this.lr = lr;
	}

	public void set_sm(simulate sm) {
		this.sm = sm;
	}

	public graphic(int space_size, boolean live) {
		this.size = space_size;
		this.live = live;

		init_fame();
		init_status_bar();
		init_cells();

		if (!live)
			init_navigation();
	}

	private void init_fame() {
		mf = new JFrame("Space");
		mf.setLayout(null);
		mf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if (live)
			mf.setSize(700, 700);
		else
			mf.setSize(800, 700);
		mf.setResizable(false);
	}

	private void init_cells() {
		space_panel = new JPanel(new GridLayout(size, size));
		space_panel.setBounds(50, 10, 580, 580);
		space_label = new JPanel[size][size];
		for (int b = 0; b < size; b++)
			for (int c = 0; c < size; c++) {
				space_label[b][c] = new JPanel();
				this.cell_h = 570 / size;
				// space_label[b][c].setSize(cell_h, cell_h);
				space_label[b][c].setBackground(Color.GRAY);
				space_label[b][c].setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
				// space_label[b][c].setLocation(c * cell_h + 50, b * cell_h +
				// 10);
				space_panel.add(space_label[b][c]);
			}
		mf.add(space_panel);
	}

	private void init_status_bar() {
		status = new JLabel("Not Started");
		status.setBounds(0, 600, 700, 30);
		status.setHorizontalAlignment(JLabel.CENTER);
		status.setVerticalAlignment(JLabel.CENTER);

		score = new JLabel("Score : 0");
		score.setBounds(100, 630, 100, 30);
		score.setHorizontalAlignment(JLabel.CENTER);
		score.setVerticalAlignment(JLabel.CENTER);

		steps = new JLabel("Steps: 0");
		steps.setBounds(200, 630, 100, 30);
		steps.setHorizontalAlignment(JLabel.CENTER);
		steps.setVerticalAlignment(JLabel.CENTER);

		passed_foods = new JLabel("Passed Food: 0");
		passed_foods.setBounds(300, 630, 150, 30);
		passed_foods.setHorizontalAlignment(JLabel.CENTER);
		passed_foods.setVerticalAlignment(JLabel.CENTER);

		mf.add(status);
		mf.add(steps);
		mf.add(score);
		mf.add(passed_foods);

	}

	public void show() {
		mf.setVisible(true);
	}

	public void update_space(int[][] space) {
		for (int j = 0; j < size; j++)
			for (int k = 0; k < size; k++) {
				if (space[j][k] == -1)
					space_label[j][k].setBackground(Color.GRAY);
				if (space[j][k] == -2)
					space_label[j][k].setBackground(Color.BLUE);
				else if (space[j][k] > 0)
					space_label[j][k].setBackground(Color.WHITE);
				else if (space[j][k] == 0)
					space_label[j][k].setBackground(Color.BLACK);
				else if (space[j][k] == -3)
					space_label[j][k].setBackground(Color.ORANGE);
			}
	}

	private void init_navigation() {
		play = new JButton("play");
		play.setBounds(630, 40, 150, 30);
		mf.add(play);
		play.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!playing) {
					play.setText("pause");
					stop.setEnabled(true);
					next.setEnabled(false);
					prev.setEnabled(false);

					lr.play();
				} else {
					play.setText("play");
					stop.setEnabled(false);
					next.setEnabled(true);
					prev.setEnabled(true);

					lr.pause();
				}
				playing = !playing;
			}
		});

		next = new JButton("next");
		next.setBounds(630, 120, 70, 30);
		mf.add(next);
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				lr.next();
			}
		});

		prev = new JButton("prev");
		prev.setBounds(710, 120, 70, 30);
		mf.add(prev);
		prev.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				lr.prev();
			}
		});

		stop = new JButton("stop");
		stop.setEnabled(false);
		stop.setBounds(630, 80, 150, 30);
		mf.add(stop);
		stop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (playing) {
					stop.setEnabled(false);
					play.setText("play");
					next.setEnabled(true);
					prev.setEnabled(true);

					playing = false;
					lr.stop();
				}
			}
		});
	}

	public void restart() {
		stop.setEnabled(false);
		play.setEnabled(true);
		play.setText("play");
		next.setEnabled(true);
		prev.setEnabled(true);
	}

	public void set_status(String status) {
		this.status.setText(status);
	}

	public void increase_step() {
		int int_step = sm.get_int_step();
		int_step++;
		sm.set_int_step(int_step);
		this.steps.setText("Steps: " + int_step);
	}

	public void set_score(double score_t) {
		int int_score = sm.get_int_step();
		int_score++;
		sm.set_int_score(int_score);
		this.score.setText("Scores: " + int_score);
	}

	public void increase_passed_food() {
		int int_passed_foods = sm.get_int_step();
		int_passed_foods++;
		sm.set_int_passed_foods(int_passed_foods);
		this.passed_foods.setText("Passed foods: " + int_passed_foods);
	}

	public void set_log_status(int step, int foods, double score) {
		this.steps.setText("Steps: " + step);
		this.passed_foods.setText("Passed foods: " + foods);
		this.score.setText("Scores: " + score);
	}
}
