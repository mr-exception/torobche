package torobche;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.JOptionPane;
import javax.swing.Timer;

public class simulate {
	// connection objects
	private ServerSocket server;
	private Socket sck;
	private Scanner in;
	private PrintStream out;
	// conditions
	private int[][] space;
	private Point head;
	private graphic gr;
	private PrintStream log_stream;
	// space situations
	private static int FOOD = -2;
	private static int ROCK = -1;
	private static int EMPTY = 0;
	// connection parameters
	private static int DONE = 0;
	private static int FAIL = 1;
	// game status
	private static int GAME_OVER = 1001;

	// game atts
	private double int_score = 0;
	private int int_step = 0;
	private int int_passed_foods = 0;

	public double get_int_score() {
		return int_score;
	}

	public void set_int_score(double int_score) {
		this.int_score = int_score;
	}

	public int get_int_step() {
		return int_step;
	}

	public void set_int_step(int int_step) {
		this.int_step = int_step;
	}

	public int get_int_passed_foods() {
		return int_passed_foods;
	}

	public void set_int_passed_foods(int int_passed_foods) {
		this.int_passed_foods = int_passed_foods;
	}

	public void set_graphic(graphic gr) {
		this.gr = gr;
	}
	private boolean init_sockets(int port) throws IOException {
		System.out.println("created a socket channel in port " + port);
		server = new ServerSocket(port);
		System.out.println("waiting for a connction...");
		sck = server.accept();
		System.out.println("connection from " + sck.getLocalAddress());
		return true;
	}
	// this function searches all the space to find HEAD of snake
	private boolean find_head() {
		for (int i = 0; i < space.length; i++)
			for (int j = 0; j < space.length; j++) {
				if (space[i][j] == 1) {
					head = new Point(i, j);
					System.out.println("found head in space(" + i + "," + j + ")");
					return true;
				}
			}
		System.out.println("head not found. wrong space");
		return false;
	}
	// start working on the socket
	public boolean run() throws IOException {
		// init the input and output streams on the socket channel
		in = new Scanner(sck.getInputStream());
		out = new PrintStream(sck.getOutputStream());
		while (sck.isConnected()) {
			// a 20 ms sleep makes rest for servers
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String msg = in.nextLine();
			System.out.println("log: " + msg);
			String[] parts = msg.split(" ");
			// msg = 'map' means get map in array
			if (parts[0].equals("map")) {
				if (space.length > 0) {
					print_map(out);
				} else
					out.print(FAIL);
			// msg = 'go X' means go to X(left, right ,up or down)
			} else if (parts[0].equals("go")) {
				gr.set_status("simulation running");
				int po = Integer.valueOf(parts[1]);
				switch (po) {
				case 0:
					System.out.println("moved up");
					if (move(head.x, head.y, head.x, head.y - 1, false)) {
						finish_one_step();
					} else {
						find_head();
						finish_game(new Point(head.x, head.y - 1));
						return true;
					}
					break;
				case 1:
					System.out.println("moved right");
					if (move(head.x, head.y, head.x + 1, head.y, false)) {
						finish_one_step();
					} else {
						find_head();
						finish_game(new Point(head.x + 1, head.y));
						return true;
					}
					break;
				case 2:
					System.out.println("moved down");
					if (move(head.x, head.y, head.x, head.y + 1, false)) {
						finish_one_step();
					} else {
						find_head();
						finish_game(new Point(head.x, head.y + 1));
						return true;
					}
					break;
				case 3:
					System.out.println("moved left");
					if (move(head.x, head.y, head.x - 1, head.y, false)) {
						finish_one_step();
					} else {
						find_head();
						finish_game(new Point(head.x - 1, head.y));
						return true;
					}
					break;
				}
			}
		}
		return true;
	}
	// do all works after one step
	private void finish_one_step() {
		find_head();
		print_map(log_stream);
		// log_stream.println();
		gr.increase_step();
		out.println(DONE);
	}

	public simulate(int port, int[][] space, graphic gr, File log) throws IOException {
		this.space = space;
		find_head();
		this.gr = gr;
		init_sockets(port);

		log_stream = new PrintStream(log);
		System.out.println("printed map in log");

	}


	// make a move on the space
	private boolean move(int hx, int hy, int dx, int dy, boolean plus) {
		// dx = dx % space.length;
		// dy = dy % space.length;
		// if (dx < 0)
		// dx += space.length;
		// if (dy < 0)
		// dy += space.length;
		// if move dest is out of space
		if (dx >= space.length || dx < 0 || dy >= space.length || dy < 0)
			return false;
		// if there is a hedge on the way
		if (space[dx][dy] != EMPTY && space[dx][dy] != FOOD)
			return false;
		int val = space[hx][hy];
		// find next part of the snake
		Point tail = find_tail(new Point(hx, hy));
		// if snake found any food(snake head crosses the food cell)
		if (space[dx][dy] == FOOD)
			plus = true;
		space[dx][dy] = val;
		space[hx][hy] = EMPTY;
		if (tail == null) {
			// add another part to snake if snake crossed over a food cell
			if (plus) {
				space[hx][hy] = val + 1;
				init.set_food(space);
				gr.increase_passed_food();
				gr.set_score(get_score());
			}
		} else {
			move(tail.x, tail.y, hx, hy, plus);
		}
		gr.update_space(space);
		return true;
	}

	private Point find_tail(Point p) {
		int value = space[p.x][p.y];
		if (p.x > 0) {
			if (space[p.x - 1][p.y] == (value + 1))
				return new Point(p.x - 1, p.y);
		} else {
			if (space[space.length - 1][p.y] == (value + 1))
				return new Point(space.length - 1, p.y);
		}
		if (p.x < space.length - 1) {
			if (space[p.x + 1][p.y] == (value + 1))
				return new Point(p.x + 1, p.y);
		} else {
			if (space[0][p.y] == (value + 1))
				return new Point(0, p.y);
		}
		if (p.y > 0) {
			if (space[p.x][p.y - 1] == (value + 1))
				return new Point(p.x, p.y - 1);
		} else {
			if (space[p.x][space.length - 1] == (value + 1))
				return new Point(p.x, space.length - 1);
		}
		if (p.y < space.length - 1) {
			if (space[p.x][p.y + 1] == (value + 1))
				return new Point(p.x, p.y + 1);
		} else {
			if (space[p.x][0] == (value + 1))
				return new Point(p.x, 0);
		}
		return null;
	}

	private void finish_game(Point p) {
		out.print(GAME_OVER);
		space[mod(p.x)][mod(p.y)] = -3;
		gr.set_status("game finished");
		gr.update_space(space);
		log_stream.println("end;");
		log_stream.println("step " + int_step);
		log_stream.println("passed_food " + int_passed_foods);
		log_stream.println("score " + int_score);
		System.out.println("simulation stoped");
	}

	private int mod(int x) {
		x = x % space.length;
		if (x < 0)
			x += space.length;
		return x;
	}

	private double get_score() {
		double result = 0;
		for (int i = 0; i < space.length; i++)
			for (int j = 0; j < space.length; j++)
				if (space[i][j] == ROCK)
					result += 2;
				else if (space[i][j] != EMPTY && space[i][j] != FOOD)
					result++;
		return result;
	}

	private void print_map(PrintStream ps) {
		for (int i = 0; i < space.length; i++) {
			ps.print(space[i][0]);
			for (int j = 1; j < space.length; j++)
				ps.print("," + space[i][j]);
			ps.print(";");
		}
		ps.println();
	}
}
